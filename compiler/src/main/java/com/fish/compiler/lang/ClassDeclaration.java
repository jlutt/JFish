package com.fish.compiler.lang;

import java.util.List;

/**
 * Created by jlutt on 2018-01-20.
 *
 * @author jlutt
 */
public class ClassDeclaration {

  private List<ScopeInstruction> commandList;

  private String className;

  public ClassDeclaration(List<ScopeInstruction> commandList, String className) {
    this.commandList = commandList;
    this.className = className;
  }

  public List<ScopeInstruction> getCommandList() {
    return commandList;
  }

  public String getClassName() {
    return className;
  }
}
