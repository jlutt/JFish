package com.fish.compiler.lang;

/**
 * Created by jlutt on 2018-01-20.
 *
 * @author jlutt
 */
public class CompilationUnit {

  private ClassDeclaration classDeclaration;

  public CompilationUnit(ClassDeclaration classDeclaration) {
    this.classDeclaration = classDeclaration;
  }

  public ClassDeclaration getClassDeclaration() {
    return classDeclaration;
  }
}
