package com.fish.compiler.lang;

import com.dny.asmtop.Command;
import com.fish.compiler.parse.domain.JFishLangVariable;

import java.util.List;

/**
 * Created by jlutt on 2018-01-22.
 *
 * @author jlutt
 */
public class VariableDeclaration implements ScopeInstruction {

  private JFishLangVariable variable;

  public VariableDeclaration(JFishLangVariable variable) {
    this.variable = variable;
  }

  @Override
  public void apply(List<Command> commandList) {
    return;
  }
}
