package com.fish.compiler.lang;

import com.dny.asmtop.Command;

import java.util.List;

/**
 * Created by jlutt on 2018-01-22.
 *
 * @author jlutt
 */
public interface ScopeInstruction {

  void apply(List<Command> commandList);
}
