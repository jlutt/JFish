package com.fish.compiler.lang;

import com.dny.asmtop.CB;
import com.dny.asmtop.Command;
import com.fish.antlr.JFishLexer;
import com.fish.compiler.parse.domain.JFishLangVariable;

import java.util.List;

/**
 * Created by jlutt on 2018-01-22.
 *
 * @author jlutt
 */
public class PrintVariable implements ScopeInstruction {

  private JFishLangVariable variable;

  public PrintVariable(JFishLangVariable variable) {
    this.variable = variable;
  }

  @Override
  public void apply(List<Command> commandList) {
    if (variable.getType() == JFishLexer.NUMBER) {
      commandList.add(CB.call(CB.fieldStatic(System.class, "out"), "println", CB.value(Integer.valueOf(variable.getValue()))));
    } else if (variable.getType() == JFishLexer.STRING) {
      commandList.add(CB.call(CB.fieldStatic(System.class, "out"), "println", CB.value(variable.getValue())));
    }
  }
}
