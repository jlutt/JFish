package com.fish.compiler.parse;

import com.dny.asmtop.Command;
import com.fish.antlr.JFishLexer;
import com.fish.antlr.JFishParser;
import com.fish.compiler.lang.CompilationUnit;
import org.antlr.v4.runtime.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Queue;

/**
 * Created by jlutt on 2018-01-20.
 *
 * @author jlutt
 */
public class SyntaxTreeTraverser {

  public CompilationUnit parseTree(String fileAbsolutePath) throws IOException {
    //fileAbolutePath - file containing first enk code file
    CharStream charStream = CharStreams.fromFileName(fileAbsolutePath, StandardCharsets.UTF_8);

    //create lexer (pass enk file to it)
    JFishLexer lexer = new JFishLexer(charStream);
    CommonTokenStream tokenStream = new CommonTokenStream(lexer);
    JFishParser parser = new JFishParser(tokenStream);

    //JFishTreeWalkListener extends EnkelBaseLitener - handles parse tree visiting events
    JFishTreeWalkListener listener = new JFishTreeWalkListener();

    //EnkelTreeWalkErrorListener - handles parse tree visiting error events
    BaseErrorListener errorListener = new JFishTreeWalkErrorListener();

    parser.addErrorListener(errorListener);
    parser.addParseListener(listener);
    parser.compilationUnit(); //compilation unit is root parser rule - start from it!

    return listener.getCompilationUnit();
  }
}
