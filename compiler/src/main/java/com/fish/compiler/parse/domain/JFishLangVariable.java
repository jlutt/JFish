package com.fish.compiler.parse.domain;

/**
 * Created by jlutt on 2018-01-20.
 *
 * @author jlutt
 */
public class JFishLangVariable {

  private int id;
  private int type;
  private String value;

  public JFishLangVariable(int id, int type, String value) {
    this.id = id;
    this.type = type;
    this.value = value;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getType() {
    return type;
  }

  public void setType(int type) {
    this.type = type;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
