package com.fish.compiler.parse;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

/**
 * Created by jlutt on 2018-01-20.
 *
 * @author jlutt
 */
public class JFishTreeWalkErrorListener extends BaseErrorListener {

  @Override
  public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
    final String errorFormat = "You fucked up at line %d,char %d :(. Details:\n%s";
    final String errorMsg = String.format(errorFormat, line, charPositionInLine, msg);
    System.out.println(errorMsg);
  }
}
