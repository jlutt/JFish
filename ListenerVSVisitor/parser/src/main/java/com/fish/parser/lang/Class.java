package com.fish.parser.lang;

import java.util.Collection;

/**
 * Created by jlutt on 2018-01-22.
 *
 * @author jlutt
 */
public class Class {

  private String name;

  private Collection<Method> methods;

  public Class(String name, Collection<Method> methods) {
    this.name = name;
    this.methods = methods;
  }

  public String getName() {
    return name;
  }

  public Collection<Method> getMethods() {
    return methods;
  }
}
