package com.fish.parser.lang;

/**
 * Created by jlutt on 2018-01-22.
 *
 * @author jlutt
 */
public class Instruction {

  private String name;

  public Instruction(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
