package com.fish.parser.lang;

import java.util.Collection;

/**
 * Created by jlutt on 2018-01-22.
 *
 * @author jlutt
 */
public class Method {

  private String name;

  private Collection<Instruction> instructions;

  public Method(String name, Collection<Instruction> instructions) {
    this.name = name;
    this.instructions = instructions;
  }

  public String getName() {
    return name;
  }

  public Collection<Instruction> getInstructions() {
    return instructions;
  }
}
