package com.fish.parser;

import com.fish.parser.lang.Class;
import com.google.gson.Gson;

/**
 * Created by jlutt on 2018-01-22.
 *
 * @author jlutt
 */
public class MainTest {

  private static final String someLangSourceCode =
      "class SomeClass {\n"+
          "    fun1 {\n"+
          "        instruction11\n"+
          "        instruction12\n"+
          "    }\n"+
          "    fun2 {\n"+
          "        instruction21\n"+
          "        instruction22\n"+
          "    }\n"+
          "}";

  public static void main(String[] args) {
    final Class result = new ListenerOrientedParser().parse(someLangSourceCode);
    Gson gson = new Gson();
    final String json = gson.toJson(result);
    System.out.printf("code below: %n '%s' %n has been parsed to object: %n '%s'%n",someLangSourceCode,json);
  }
}
