// Generated from D:/Work/JFish/antlr/src/main/java/com/fish/antlr\JFish.g4 by ANTLR 4.7
package com.fish.antlr;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link JFishParser}.
 */
public interface JFishListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link JFishParser#compilationUnit}.
	 * @param ctx the parse tree
	 */
	void enterCompilationUnit(JFishParser.CompilationUnitContext ctx);
	/**
	 * Exit a parse tree produced by {@link JFishParser#compilationUnit}.
	 * @param ctx the parse tree
	 */
	void exitCompilationUnit(JFishParser.CompilationUnitContext ctx);
	/**
	 * Enter a parse tree produced by {@link JFishParser#classDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterClassDeclaration(JFishParser.ClassDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link JFishParser#classDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitClassDeclaration(JFishParser.ClassDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link JFishParser#className}.
	 * @param ctx the parse tree
	 */
	void enterClassName(JFishParser.ClassNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link JFishParser#className}.
	 * @param ctx the parse tree
	 */
	void exitClassName(JFishParser.ClassNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link JFishParser#classBody}.
	 * @param ctx the parse tree
	 */
	void enterClassBody(JFishParser.ClassBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link JFishParser#classBody}.
	 * @param ctx the parse tree
	 */
	void exitClassBody(JFishParser.ClassBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link JFishParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(JFishParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link JFishParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(JFishParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link JFishParser#print}.
	 * @param ctx the parse tree
	 */
	void enterPrint(JFishParser.PrintContext ctx);
	/**
	 * Exit a parse tree produced by {@link JFishParser#print}.
	 * @param ctx the parse tree
	 */
	void exitPrint(JFishParser.PrintContext ctx);
	/**
	 * Enter a parse tree produced by {@link JFishParser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(JFishParser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link JFishParser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(JFishParser.ValueContext ctx);
}