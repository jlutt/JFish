未实现完全

## 开发记录
> - 2018-01-20 基本的代码，实现var和print [版本1](https://gitee.com/jlutt/JFish/tree/d9f4e3e6df3709a42c3a6ee4034ff66379cf5717/)

```
var five=5
print five
var dupa="dupa"
print dupa
```

> - 2018-01-22 实现class
[版本2](https://gitee.com/jlutt/JFish/tree/e7c97da94243ed3408cb1f7944e139db0e632295/)
```
Car {
    var five=6
    print five
    var dupa="jlutt"
    print dupa
}
```

> - 2018-01-22 比较antlr4的两种处理方式listen和visitor
[版本3](https://gitee.com/jlutt/JFish/tree/760e7a4161206bd8aa144faa6359a14edaf87d97/)
